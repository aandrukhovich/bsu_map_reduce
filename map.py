import fileinput
for line in fileinput.input():
    words = line.split(' ')
    for w in words:
        w = w.strip()
        if (len(w) == 0):
            continue
        if (w[-1] == '\n'):
            w = w[:-1]
        print(w, str(1), sep='\t')
    print()