# Важный алёрт

Пока что шаг map не работает на больших данных, на примерно 27к символов зависает выполнение execvp с map-скриптом. можете не тестить на больших данных.

# MapReduce

Проект MapReduce.

## Getting Startedct

Склонируйте этот репозиторий, например, git clone-ом, думаю, вы это умеете.

### Prerequisites

Хорошо бы иметь linux, g++ и python3. Если их нет - плоховато.


### Installing

После того, как склонили реп, надо собрать lab1.cpp:

```
g++ -std=c++11 lab1.cpp -o lab1
```

## Usage

Пусть map.py и reduce.py - скрипты на питоне, читающие из stdin и пищущие в stdout (в репе лежат примеры для wordcount).

### Для операции map выполните:

```
./lab1 map map.py in.txt out.txt
```

где in.txt -- исходный файл, который будет передан map.py; out.txt -- файл, где будет храниться вывод map.py

В репе лежат примеры для входного файла : alphabet.txt (с алфавитом) и in.txt (с рандомными словами).

### Для операции reduce выполните:

```
./lab1 reduce reduce.py out.txt ans.txt
```

Где out.txt -- вывод map.py, ans.txt -- файл, где будут храниться выводы запусков reduce.py



<img src="https://pp.userapi.com/c638617/v638617761/31d68/GznPQ8gJMGY.jpg" width="400" height="400" />

