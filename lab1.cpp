//
//  Sorry for this code
//  I'll rewrite it later :)
//

#include <cstdio>
#include <string>
#include <vector>
#include <cstring>
#include <iostream>
#include <unistd.h>
#include <stdlib.h>
#include <fstream>
#include <sys/wait.h>
#include <sstream>
#include <algorithm>


constexpr size_t SIZE_FOR_BUF = 2000000;
constexpr size_t B = 2000000;
constexpr size_t MAX_SIZE_KEY = 25;
constexpr size_t MAX_SIZE_VALUE = 4;
constexpr size_t MAX_B = B + MAX_SIZE_VALUE + MAX_SIZE_KEY + 2;


void print_bad() {
    std::cout << "bad arguments of command line \n";
    exit(1);
}


void empty_file() {
    std::cout << "file is empty";
    exit(1);
}


void sort(std::string src_file) {
    std::ifstream in(src_file);
    std::vector<std::pair<std::string, size_t>> file;

    std::string key;
    size_t value;
    while (in >> key) {
        in >> value;
        file.emplace_back(key, value);
    }
    in.close();

    if (file.empty()) {
        empty_file();
    }

    std::sort(file.begin(), file.end());

    std::ofstream out(src_file);
    for (auto p : file) {
        out << p.first << "\t" << p.second << "\n";
    }
    out.close();
}
std::vector<std::string> read_block(std::ifstream& file) {
    std::vector<std::string> result;

    char* buf = new char[MAX_B];

    size_t len = file.readsome(buf, B);
    if (len == 0) {
        delete[] buf;
        return result;
    }
    size_t last = len - 1;
    while (buf[last] != '\n') {
        file.read(buf + last + 1, 1);
        last++;
    }
    buf[last + 1] = 0;
    char* pch = strtok(buf, "\n");
    while (pch != 0) {
        result.emplace_back(pch);
        pch = strtok(0, "\n");
    }

    delete[] buf;
    return result;
}


std::pair<std::string, std::string> string_to_kv(std::string s) {
    size_t pos_t = s.find('\t');
    return std::make_pair(s.substr(0, pos_t), s.substr(pos_t + 1, s.size()));
}


std::string kv_to_string(std::pair<std::string, std::string> p) {
    return std::string(p.first + "\t" + p.second + "\n");
}


size_t count_files = 0;
std::string get_new_name() {
    return std::string(std::to_string(count_files) + ".txt");
}

void partial_sort(std::string src_file) {
    std::ifstream in(src_file);
    auto block = read_block(in);
    while (!block.empty()) {
        std::vector<std::pair<std::string, std::string>> kv_block;
        for (auto i : block) {
            kv_block.emplace_back(string_to_kv(i));
        }

        std::sort(kv_block.begin(), kv_block.end());
        block.clear();

        std::string new_name = get_new_name();
        count_files++;
        std::ofstream out(new_name);

        for (auto i : kv_block) {
            out << kv_to_string(i);
        }

        block = read_block(in);
    }
}

void merge_and_remove(std::string first_file, std::string second_file,
                      std::string result_file) {

    std::ifstream f_stream(first_file);
    std::ifstream s_stream(second_file);
    std::ofstream o_stream(result_file);

    std::string f_head, s_head;
    std::getline(f_stream, f_head);
    std::getline(s_stream, s_head);

    while (f_stream && s_stream) {
        std::string f_head_key = string_to_kv(f_head).first;
        std::string s_head_key = string_to_kv(s_head).first;

        if (f_head_key < s_head_key) {
            o_stream << f_head << '\n';
            std::getline(f_stream, f_head);
        } else {
            o_stream << s_head << '\n';
            std::getline(s_stream, s_head);
        }
    }

    while (f_stream) {
        o_stream << f_head << '\n';
        std::getline(f_stream, f_head);
    }

    while (s_stream) {
        o_stream << s_head << '\n';
        std::getline(s_stream, s_head);
    }
    remove(first_file.c_str());
    remove(second_file.c_str());
}


void unary_merge(size_t index) {
    std::string before = std::to_string(index) + ".txt";
    std::string after = std::to_string(index) + '-'
                        + std::to_string(index) +  ".txt";
    std::rename(before.c_str(), after.c_str());
}



std::string get_name(size_t l, size_t r) {
    return std::to_string(l) + '-' + std::to_string(r) + ".txt";
}

void binary_merge(size_t first_left, size_t first_right,
                  size_t second_left, size_t second_right) {

    merge_and_remove(get_name(first_left, first_right),
                     get_name(second_left, second_right),
                     get_name(first_left, second_right));

}


void recursive_merge(size_t left, size_t right) {
    if (left == right) {
        unary_merge(left);
    } else {
        size_t med = (left + right) / 2;
        recursive_merge(left, med);
        recursive_merge(med + 1, right);
        binary_merge(left, med, med + 1, right);
    }
}

void good_sort(std::string src_file) {

    std::cerr << "partial_sort   \n";
    partial_sort(src_file);
    std::cerr << "recursive_merge   \n";
    recursive_merge(0, count_files - 1);
    std::cerr << "rename   \n";

    std::string before = "0-" + std::to_string(count_files - 1) +  ".txt";
    std::rename(before.c_str(), src_file.c_str());
    std::cerr << "sort complete   \n";
}

int main(int argc, char** argv) {
    if (argc < 5) {
        print_bad();
    }

    std::string operation = argv[1];
    std::string path_to_script = argv[2];
    std::string src_file = argv[3];
    std::string dst_file = argv[4];

    if (operation == "map") {

        int parent_child[2];
        int child_parent[2];
        pipe(parent_child);
        pipe(child_parent);

        std::ofstream output(dst_file);
        std::ifstream input(src_file);
        std::string s;
        size_t count_strings = 0;
        while (std::getline(input, s)) {
            s += '\n';

            write(parent_child[1], s.data(), s.size());
            pid_t pid = fork();

            if (pid == 0) {
                close(parent_child[1]);
                close(child_parent[0]);
                dup2(parent_child[0], 0);
                dup2(child_parent[1], 1);

                char* path = new char[path_to_script.length() + 1];
                strcpy(path, path_to_script.c_str());

                char *newargv[] = {(char*)("python3"), path, NULL };
                if (execvp(newargv[0], newargv)) {
                    perror("execvp");
                }
                exit(1);
            }

            close(parent_child[1]);
            wait(NULL);

            char buf[SIZE_FOR_BUF];
            memset(buf, 0, SIZE_FOR_BUF);
            read(child_parent[0], buf, sizeof(buf));
            output << buf;
            count_strings++;
            std::cerr << count_strings << '\n';

            pipe(parent_child);
            pipe(child_parent);
        }

        std::cerr << "map is finished\n";

    } else if (operation == "reduce") {

        good_sort(src_file);

        int parent_child[2];
        int child_parent[2];
        pipe(parent_child);
        pipe(child_parent);

        std::ofstream final(dst_file);
        std::ifstream sort_in(src_file);
        std::string s;

        std::getline(sort_in, s);
        s.push_back('\n');
        std::string last = s.substr(0, s.find('\t'));
        ssize_t x = write(parent_child[1], s.data(), s.size());

        while (std::getline(sort_in, s)) {
            s.push_back('\n');
            std::string key = s.substr(0, s.find('\t'));

            if (key == last) {
                ssize_t x = write(parent_child[1], s.data(), s.size());
                continue;
            }
            last = key;

            pid_t pid = fork();
            if (pid == 0) {
                close(parent_child[1]);
                close(child_parent[0]);
                dup2(parent_child[0], 0);
                dup2(child_parent[1], 1);

                char* path = new char[path_to_script.length() + 1];
                strcpy(path, path_to_script.c_str());

                char *newargv[] = {(char*)("python3"), path, NULL};
                if (execvp(newargv[0], newargv)) {
                    perror("execvp");
                }
                exit(1);
            }

            close(parent_child[1]);
            wait(NULL);

            char buf[SIZE_FOR_BUF];
            memset(buf, 0, SIZE_FOR_BUF);
            read(child_parent[0], buf, sizeof(buf));

            final << buf;

            pipe(parent_child);
            pipe(child_parent);

            ssize_t x = write(parent_child[1], s.data(), s.size());
        }

        pid_t pid = fork();
        if (pid == 0) {
            close(parent_child[1]);
            close(child_parent[0]);
            dup2(parent_child[0], 0);
            dup2(child_parent[1], 1);

            char* path = new char[path_to_script.length() + 1];
            strcpy(path, path_to_script.c_str());

            char *newargv[] = {(char*)("python3"), path, NULL};
            if (execvp(newargv[0], newargv)) {
                perror("execvp");
            }
            exit(1);
        }

        close(parent_child[1]);
        wait(NULL);

        char buf[SIZE_FOR_BUF];
        memset(buf, 0, SIZE_FOR_BUF);
        read(child_parent[0], buf, sizeof(buf));

        final << buf;

        std::cerr << "reduce is finished\n";

    } else {
        print_bad();
    }

    return 0;
}